from django.test import TestCase
from django.urls import reverse, resolve

#view example
def example_view():
    pass

class TestGenericView(TestCase):
    """
    This is generic testcase using django TestCase class. With url is index, 
    view is example_view and template is example_template.html.
    Class name always begin with 'Test'.
    """
    def setUp(self):
        self.url = reverse('index')
        self.response = self.client.get(self.url)

    def test_status_code(self):
        self.assertEqual(self.response.status_code,200)

    def test_url_show_correct_func(self):
        view = resolve('/index/')
        self.assertEqual(view.func.__name__, example_view.__name__)

    def test_view_show_correct_template(self):
        self.assertTemplateUsed(self.response, 'example_template.html')
