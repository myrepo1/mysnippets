from django.test import TestCase
from django.urls import reverse, resolve

#view example
def example_view():
    pass

class ExampleForm():
    pass

class TestGenericView(TestCase):
    """
    This is generic form testcase using django TestCase class. With url is formpage, 
    view is example_view, template is example_template.html and form class is ExampleForm.
    Class name always begin with 'Test'.
    """
    def setUp(self):
        self.url = reverse('formpage')
        self.response = self.client.get(self.url)

    def test_status_code(self):
        """
        Test response return 200 status code
        """
        self.assertEqual(self.response.status_code,200)

    def test_url_show_correct_func(self):
        """
        Test resolve result show correct view. A view could be from method or class
        """
        view = resolve('/formpage/')
        self.assertEqual(view.func.__name__, example_view.__name__)

    def test_view_show_correct_template(self):
        """
        Test response show correct template
        """
        self.assertTemplateUsed(self.response, 'example_template.html')

    def test_view_show_correct_form(self):
        """
        Test response using correct form
        """
        form = self.response.context.get('form')
        self.assertIsInstance(form, ExampleForm)

    def test_form_use_csrf(self):
        """
        Test response using csrf token
        """
        self.assertContains(self.response,'csrfmiddlewaretoken')

