// This is script for dynamic formset

function toggle_row_input_availability() {
    // Toggle formset delete status 
    const active_button = event.srcElement;
    const delete_input_element = active_button.nextElementSibling;
    const parent_element = active_button.parentElement.parentElement;
    if(active_button.classList.contains('modify-button-disabled')) {
        delete_input_element.checked = false;
        Array.prototype.forEach.call(parent_element.children, (item)=>{
            Array.prototype.forEach.call(item.children, (child)=>{
                if(child.nodeName=='INPUT'||child.nodeName=='SELECT') {
                    child.disabled = false;
                }
            });
        });
        active_button.classList.remove('modify-button-disabled');
        active_button.classList.add('modify-button');
    } else if(active_button.classList.contains('new-modifiy-button')) {
        parent_element.parentNode.removeChild(parent_element);
        const required_attributes = delete_input_element.getAttribute('name').split('-');
        const prefix = required_attributes[0];
        $(`#id_${prefix}-TOTAL_FORMS`)[0].value = +($(`#id_${prefix}-TOTAL_FORMS`)[0].value) -1;
    } else {
        delete_input_element.checked = true;
        active_button.classList.remove('modify-button');
        active_button.classList.add('modify-button-disabled');
    }
}

function add_new_row_input() {
    const active_button = event.srcElement;
    const delete_input_element = active_button.nextElementSibling;
    // const fields = $('.formset-generic')[0].getAttribute('fields_list').split('__').slice(1);
    const required_attributes = delete_input_element.getAttribute('name').split('-');
    const prefix = required_attributes[0];
    const num = required_attributes[1];
    const new_element = active_button.parentElement.parentElement.cloneNode(true);
    Array.prototype.forEach.call(new_element.children, (item)=>{
        Array.prototype.forEach.call(item.children, (child)=>{
            if(child.nodeName == 'SPAN') {
                child.innerHTML = +child.innerHTML +1;
            } else if(child.nodeName=='INPUT'||child.nodeName=='SELECT') {
                const initial_name = child.getAttribute('name').split(num);
                const new_id = 'id_'+initial_name[0] + (+num+1) + initial_name[1];
                const new_name = initial_name[0] + (+num+1) + initial_name[1];
                child.id = new_id;
                child.setAttribute('name',new_name);
                child.value = '';
            }
        });
    });
    $('#formsetTableBody')[0].appendChild(new_element);
    active_button.classList.remove('btn','btn-success','add-button');
    active_button.children[0].classList.remove('fa-plus');
    active_button.classList.add('btn','btn-danger','new-modifiy-button');
    active_button.children[0].classList.add('fa-trash');
    active_button.setAttribute('onclick','toggle_row_input_availability()');
    $(`#id_${prefix}-TOTAL_FORMS`)[0].value = +($(`#id_${prefix}-TOTAL_FORMS`)[0].value) +1;
}
