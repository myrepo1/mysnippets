from django import forms
"""
--------
Dynamic Formset with vanilla javascript. With this formset and its javascript functions, form
could be added or removed dynamicly.
--------
"""

class SingleFormModel(forms.Form):
    """
    Single form for each form in formset
    """
    name = forms.CharField()
    address = forms.CharField()

class BaseFormset(forms.BaseFormSet):
    """
    Baseformset to be used with single form
    """

    def add_fields(self,form,index):
        super().add_fields(form,index)
        #Add class delete-input to DELETE field to be used with javascript
        form.fields['DELETE'].widget.attrs.update({'class':'delete-input'})

#Set can_delete to true, to enable delete form instance
GenericFormset = forms.formset_factory(SingleFormModel,BaseFormset,extra=1,can_delete=True)