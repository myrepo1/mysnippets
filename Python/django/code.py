def generate_excel(data,headers,title,filename):
    """
    Generate excel file from a queryset and return the file to response
    without saving it to server.
    Parameters
    -------
    data: list of dictionary or queryset
        data to be converted into excel file
    headers: list
        list of field names on top of sheet
    title: str
        name of sheet
    filename: str
        name of excel file
    Return
    -------
        Return response with excel file in attachment
    """

    #Import required module
    from openpyxl import Workbook
    from openpyxl.writer.excel import save_virtual_workbook
    from django.http import HttpResponse

    #Create workbook
    wb = Workbook()
    ws = wb.active
    ws.title = title
    headers = headers
    data_length = len(data)
    first_line = True
    #Row begin from index 1 because first row index in excel start from one
    #Row end in data_length+2 because first row filled with header and first row index is start from 1
    for row in range(1,data_length+2):
        if first_line:
            #Col begin from index 1 because first col index start from one
            for col in range(1,len(headers)+1):
                ws.cell(row=row,column=col,value=headers[col-1])
            first_line = False
            continue
        else:
            data_item = data[row-2]
            for col in range(1,len(headers)+1):
                #Get value in data if exists
                cell_value = data_item[headers[col-1]] if headers[col-1] in data_item else None
                ws.cell(row=row,column=col,value=cell_value)
    file_result = f'{filename}.xlsx'
    #Return excel file to response using save_virtual_workbook function
    response = HttpResponse(content=save_virtual_workbook(wb), content_type='application/ms-excel')
    response['Content-Disposition'] = f'attachment; filename={file_result}'
    return response