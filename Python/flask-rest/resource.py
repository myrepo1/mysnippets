"""
This script contain all resource to serialize, or deserialize and validate data.
"""
import copy
from flask import request
from flask_restful import Api, Resource
from marshmallow import Schema, fields, ValidationError
from werkzeug.exceptions import HTTPException


class CustomApi(Api):
    """
    CustomAPI add custom method to flask_restful Api class
    Currently, there is one method added.
    """

    def handle_error(self,err):
        """It helps preventing writing unnecessary
        try/except block through out the application
        Parameters:
        -----
        err:
            err object.
        Returns:
        -----
            return error response.
        """
        error_response = {'code':400,'status':False,'message':'','detail_message':{},'data':{}}
        if err.code == 405:
            error_response['code'] = 405
            error_response['message'] = 'Method Not Allowed.'
        elif isinstance(err,HTTPException):
            error_response['code'] = err.code
            if 'message' in err.data and err.data['message']:
                error_response['message'] = err.data['message']
            else:
                error_response['message'] = err.data['description']
        else:
            error_response['code'] = 500
            error_response['message'] = 'Server has encountered some error.'
        return error_response,500

class CustomResource(Resource):
    """
    CustomResource inherit from flask_restful Resource class.
    This class add some method to (de)serialize data.
    """
    def __init__(self,schema):
        self.schema = copy.deepcopy(schema)
        self.errors = {}
        self.exclude = []

    def get_value(self,data,key,default=None):
        """
        Get value if exist from data based on its key.
        Return default value if key not found.
        Parameters:
        -----
        data:dict
            Data to be searched.
        key;str
            key expected to be found.
        default:object
            Default value to be returned, if key not found.
        Returns:
            Return value if key found in data, else return default.
        """
        try:
            return data[key]
        except KeyError:
            return default

    def add_error(self,field,error_message):
        """
        Add error message to self.errors, set error key to
        '__all__' if field is None.
        Parameters:
        -----
        field:str
            Error field to be added.
        error_message:
            Error message
        """
        error_field = '__all__' if field is None else field
        if error_field in self.errors:
            if isinstance(error_message,list):
                self.errors[error_field].extend(error_message)
            else:
                self.errors[error_field].append(error_message)
        else:
            if isinstance(error_message,list):
                self.errors[error_field] = [*error_message,]
            else:
                self.errors[error_field] = [error_message,]
    
    def normalize_error(self):
        """
        Convert dict inside fields in self.errors to list,
        so there is no sub-dictionary
        """
        for key in self.errors:
            if isinstance(self.errors[key],dict):
                normalized_message = []
                for i in self.errors[key]:
                    normalized_message.extend(self.errors[key][i])
                self.errors[key] = normalized_message

    def add_exclude(self,values):
        """
        Add excluded field to marshmallow data schema.
        Parameters:
        -----
        values:list
            List of fieldname to be added.
        """
        self.exclude.extend(values)

    def get_dict_schema(self,raw_data,is_post=False):
        """
        Generate marshmallow dictionary schema for payload.
        Parameters:
        -----
        raw_data:dict
            schema raw_data, similar in self.schema.
        is_post:bool
            If this true, add required options for field with post_required True.
        Returns:dict
        -----f
            Marshmallow schema ready to be generated.
        """
        schema_result = {}
        for name,item in raw_data.items():            
            options = {k:v for k,v in item['options'].items()}
            options['required'] = is_post and self.get_value(item,'post_required')
            if self.get_value(item,'nested'):                
                sub_schema = self.get_dict_schema(item['source'],is_post=is_post)
                if self.get_value(item,'many',False):
                    schema_result[name] = fields.List(item['field'](Schema.from_dict(sub_schema)),**options)
                else:
                    schema_result[name] = item['field'](Schema.from_dict(sub_schema),**options)
            else:
                schema_result[name] = item['field'](**options)
        return schema_result

    def get_schema(self,is_post=False,many=False):
        """
        Generate marshmallow schema to (de)serialize. Initial data from self.schema with attribute is_payload True.
        Parameters:
        -----
        is_post:bool
            If this True, schema will check for post_required attribute, to add required options in schema.
        many:bool
            If this True, generated schema options will be add many as True.
        Returns:
            Marshmallow schema object.
        """
        raw_data = {k:v for k,v in self.schema.items() if self.get_value(v,'is_payload')}
        schema_dict = self.get_dict_schema(raw_data,is_post=is_post)
        return Schema.from_dict(schema_dict)(many=many,unknown='EXCLUDE',exclude=self.exclude)

    def get_query_schema(self):
        """
        Generate query schema, initial data from self.schema with attribute is_query True.
        Returns:
        -----
            Marshmallow schema object.
        """
        schema_result = {}
        raw_query_schema = {k:v for k,v in self.schema.items() if self.get_value(v,'is_query')}
        for name,item in raw_query_schema.items():
            if self.get_value(item,'query_options'):
                options = {k:v for k,v in item['query_options'].items()}
            else:
                options = {}
            schema_result[name] = fields.List(item['field'](**options))
        return Schema.from_dict(schema_result)(unknown='EXCLUDE')

    def load_data(self, schema,data):
        """
        Deserialize data with marshmallow schema.
        Parameters:
        -----
        schema:
            Marshmallow schema object.
        data:dict
            Data to be deserialized
        Returns:dict
        -----
            Deserialized data.
        """
        return schema.load(data)

    def dump_data(self,data):
        """
        Serialize data with marshmallow schema.
        Parameters:
        data:dict
            Data to be serialized.
        Returns:dict
        -----
            Serialized data.
        """
        schema = self.get_schema(self.schema,many=isinstance(data,list))
        return schema.dump(data)

    def validate_data(self,schema,data):
        """
        Validate data against marshmallow schema. Set errors result to self.errors object.
        Parameters:
        schema:
            Marshmallow schema object
        data:dict
            Data to be validated
        Returns:bool
        -----
            Return true if data is valid.
        """
        self.errors = schema.validate(data)
        if any(self.errors):
            return False
        else:
            return True
    
    def get_cleaned_data(self,is_post=False):
        """
        Get deserialized data that has been validated.
        Parameters:
        -----
        is_post:bool
            Is payload is a post data.
        Returns:dict
        -----
            Deserialized data.
        """
        if request.is_json:
            raw_data = request.json
        else:
            raw_data_list = dict(request.args.lists())
            raw_data = {k:v[0] for k,v in raw_data_list.items()}
        schema = self.get_schema(is_post=is_post)
        for k in raw_data:
            if isinstance(raw_data[k],str) and not raw_data[k]:
                raw_data[k] = None
        if self.validate_data(schema,raw_data):
            return self.load_data(schema,raw_data)
    
    def get_raw_query(self):
        """
        Get query from flask's request object.
        Returns:dict
        -----
            Raw query data.
        """
        params = request.args
        params_non_flat = params.to_dict(flat=False)
        result = {}
        for k,v in params_non_flat.items():
            if len(v)>1:
                result[k] = v
            elif ',' in v[0]:
                result[k] = v[0].split(',')
            else:
                result[k] = v[0]
        return result

    def get_cleaned_query(self):
        """
        Get validated query data.
        Returns:dict
        -----
            Return validated query data.
        """
        raw_data = self.get_raw_query()
        parsed_raw_query = {}
        field_list = {}
        result = {}
        for k,v in raw_data.items():
            field_list_name = k.split('__')
            field_name = field_list_name[0]
            try:
                if isinstance(v,list):
                    parsed_raw_query[field_name].extend(v)
                else:
                    parsed_raw_query[field_name].append(v)
            except KeyError:
                if isinstance(v,list):
                    parsed_raw_query[field_name] = [*v,]
                else:
                    parsed_raw_query[field_name] = [v,]
            try:
                field_list[field_name].append(k)
            except KeyError:
                field_list[field_name] = [k,]
        schema = self.get_query_schema()
        self.errors = schema.validate(parsed_raw_query)
        if not any(self.errors):
            validated_data = self.load_data(schema,parsed_raw_query)
            for field in field_list:
                names = field_list[field]
                try:
                    values = validated_data[field]
                except KeyError:
                    continue
                result = {**result,**dict(zip(names,values))}
        return result

    def get_column_names(self):
        """
        Get column's names from self.schema.
        Returns:list
        -----
            List of column's names.
        """
        return [k for k,v in self.schema.items() if self.get_value(v,'is_payload') and not self.get_value(v,'additional')]

    def get_current_host(self):
        """
        Get current request's host_url.
        Return:str
        -----
            Host url.
        """
        return request.host_url

    def get_response(self,code=200,status=True,message='',data=None,additional_data=None,no_schema=False,is_many=False):
        """
        Generate response with formatted data using self.schema.
        Parameters:
        -----
        code:int
            HTTP Status Code
        status:bool
            True if there is no error in response.
        message:str
            Response message.
        data:list,dict
            Data to be formatted.
        additional_data:dict
            Additional or meta information, such as page, total page.
        no_schema:bool
            If this True, formatted_data use data as it is, without self.schema's dump_method.
        is_many:bool
            If this True, formatted_data will be always a list, event if Data is None, used to keep consistent format.
        Returns:
        -----
            Response with its HTTP Status Code.
        """
        if no_schema:
            formatted_data = data
        elif not data and is_many:
            formatted_data = []
        elif not data:
            formatted_data = {}
        else:
            formatted_data = self.dump_data(data)
        self.normalize_error()
        response = {
            'code':code,
            'status':status,
            'message':message,
            'detail_message':self.errors,
            'data':formatted_data
            }
        response['additional_data'] = additional_data if additional_data else {}
        return response,code