from sqlalchemy import Column

def check_field_exist(raw_field,columns):
    """
    Check if field exist in a table, using column name list of the table.
    Parameters
    -----
    raw_field:str
        Field possible combined with query operator, such as gte or like.
    columns:list
        List of column name in a model.
    Returns
    -----
        True if field exist in a table
    """
    field_list = raw_field.split('__')
    return field_list[0] in columns

def generate_field_filter(model,raw_field,value):
    """
    Generate sqlalchemy comparison based on query operator in raw_field.
    If raw field has no query operator, return equality comparison (==)
    Parameters
    -----
    model:sqlalchemy model
        Sqlalchemy model to retrieve field for comparison.
    raw_field:str
        Field possible combined with query operator, such as gte or like.
    value:any
        Value used in field comparison.
    Returns
    -----
        Return sqlalchemy comparison
    """
    field_list = raw_field.split('__')
    field = getattr(model,field_list[0],None)
    if not field:
        return field
    if len(field_list)>1:
        if field_list[1]=='gte':
            return field>=value
        elif field_list[1]=='gt':
            return field>value
        elif field_list[1]=='lt':
            return field<value
        elif field_list[1]=='lte':
            return field<=value
        elif field_list[1]=='like':
            return field.like(value)
        elif field_list[1]=='in':
            return field.in_(value)
        else:
            return field==value
    else:
        return field==value

def generate_filter(model,query):
    """
    Create dynamic query filter based on field operator. Unpack
    the result in filter method in query.
    Parameters
    -----
    model:sqlalchemy model
        Sqlalchemy model to retrieve field for comparison.
    query:dict
        Dict of query used in filter field generator.
    Returns
    -----
        Return list of sqlalchemy comparison
    """
    column_list = [item.name for item in model.__table__.columns]
    result = []
    for k,v in query.items():
        if check_field_exist(k,column_list):
            field_filter = generate_field_filter(model,k,v)
            result.append(field_filter)
    return result