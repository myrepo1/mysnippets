from sqlachemy import func

class HistoryMixin:
    """
    Abstract class to provide version control of records in table.
    The idea is only 1 record with certain id and end_date empty
    may exist. For create, update, delete process, it use custom
    add_data,update_data and delete_data.
    """
    record_id = db.Column(db.Integer,primary_key=True)
    data_id = db.Column(db.Integer, index=True,nullable=False)
    start_date = db.Column(db.DateTime, default=datetime.utcnow, index=True,nullable=False)
    end_date = db.Column(db.DateTime, index=True,nullable=True)
    start_by = db.Column(db.Integer,nullable=False)
    ended_by = db.Column(db.Integer, nullable=True)
    start_note = db.Column(db.Text, nullable=True)
    end_note = db.Column(db.Text, nullable=True)
    start_mode = db.Column(db.Integer, index=True,nullable=False)
    end_mode = db.Column(db.Integer, index=True, nullable=True)


class CustomORM:
    """
    Class to collect method related with ORM using HistoryMixin.
    """
    def __init__(self,model):
        """
        Class initialization
        Parameters:
        -----
        model: SQLAlchemy Table Model
        """
        self.model = model
        self.db = db

    def get_current_record(self,data_id):
        """
        Get current record that still active or end_date is None.
        Parameters:
        -----
        data_id:
            record id in database.
        Returns:
            Record object if exist
        -----
        """
        return self.model.query.filter(self.model.data_id==data_id,self.model.end_date==None).first()

    def get_current_records(self,query_filter=[]):
        """
        Get all records that still active or end date is None.
        Parameters:
        -----
        query_filter: list
            list of filter arguments to be used in filter method.
        Returns:
            Query result which contain all active records.
        """
        return self.model.query.filter(self.model.end_date==None,*query_filter).all()
    
    def get_record_history(self,data_id):
        """
        Get all records with certain data_id.
        Use this method to get history of certain record.
        parameters:
        -----
        self: Model object
            Model object to be used in query
        data_id:
            record id in database.
        Returns:
        -----
            Query result which contain all records with certain record id.
        """
        return self.model.query.filter(self.model.data_id==data_id).all()

    def add_data(self,data,user_id,start_mode=1):
        """
        Add record to database. If this is a new record,
        generate id for its record.
        Parameters:
        -----
        data: dict
            Data to be added to database.
        user_id: int
            Current user id who perform this operation.
        start_mode: int
            Adding record mode, 1 for create record, 2 for update record.
        """
        if start_mode==1:
            last_id = self.db.session.query(func.max(self.model.data_id)).first()[0]
            data['data_id'] = last_id+1 if last_id else 1
        data['start_by'] = user_id if user_id else 0
        data['start_mode'] = start_mode
        if 'note' in data:
            data['start_note'] = data['note']
        record = self.model(**data)    
        self.db.session.add(record)
        return record

    def add_data_bulk(self,data,user_id=None):
        """
        Add multiple records into a table. 
        If user_id exist, addition is consider as adding new data, else it's an update.
        Parameters:
        -----
        data:list
            List of dictionary contain data to be added.
        user_id:int
            user id in database.
        """
        if user_id:
            last_id = self.db.session.query(func.max(self.model.data_id)).first()[0]
            for item in data:
                last_id = last_id+1 if last_id else 1
                item['data_id'] = last_id
                item['start_by'] = user_id
                item['start_mode'] = 1
                item['start_date'] = datetime.utcnow()                
        self.db.session.bulk_insert_mappings(self.model,data)

    def delete_data(self,record,user_id,end_date=datetime.utcnow(),note=None,end_mode=1):
        """
        Soft delete record by setting its end_date to current utctime.
        If this deletion is part of updating process, get current record value,
        except its record_id.
        Parameters:
        -----
        record: object
            Current record to be deleted.
        user_id: int
            Current user id who perform this operation.
        end_date: datetime
            time of deletion.
        note: str
            note for deletion.
        end_mode: int
            Delete record mode, 1 for delete record, 2 for update record.
        Return: dict
        -----
        Return current record values if end_mode == 2, else return record id.
        """
        data = {}
        if end_mode == 2:
            data = {x.name:getattr(record,x.name) for x in record.__table__.columns}
            data.pop('record_id')
        else:
            data['data_id'] = record.data_id
        record.end_date = end_date
        record.end_by = user_id if user_id else 0
        record.end_note = note
        record.end_mode = end_mode
        return data
    
    def delete_data_bulk(self,filter_data,user_id,end_date=None):
        """
        Soft delete multiple record in database. If end_date exist
        deletion process is considered as part of update process.
        Parameters:
        -----
        filter_data:tuple
            Tuple of filter item comparison, such as end_date==None.
        user_id:int
            user_id of user who commit this deletion process.
        end_date:datetime
            time of deletion in utc.
        """
        update_data = {'ended_by':user_id}
        update_data['end_date'] = end_date if end_date else datetime.utcnow()
        update_data['end_mode'] = 2 if end_date else 1
        self.db.session.query(self.model).filter(*filter_data).update(update_data)

    def update_data(self,record,data,user_id):
        """
        Update record by soft delete it, copy all 
        its current value and combine it with new data,
        and generate new record for those new data.
        Parameters:
        -----
        record: object
            Current record to be deleted.
        data: dict
            Data to be updated to database.
        user_id: int
            Current user id who perform this operation.
        Return:
        -----
            Return record object with updated data.
        """
        end_date = datetime.utcnow()
        note = data.pop('note',None)
        prev_data = self.delete_data(record,user_id,end_date=end_date,note=note,end_mode=2)
        for k,v in data.items():
            prev_data[k] = v
        prev_data['start_date'] = end_date
        prev_data['start_by'] = user_id if user_id else 0
        prev_data['start_note'] = note
        return self.add_data(prev_data,user_id,start_mode=2)

    def update_data_bulk(self,filter_data,update_data,user_id):
        """
        Update multiple records in database based on filter_data.
        Parameters:
        -----
        filter_data:tuple
            Tuple of filter item comparison, such as end_date==None.
        update_data:dict
            Dict of item,value to be updated.
        user_id:int
            user_id of user who commit this deletion process.
        """
        query_data = self.model.query.filter(*filter_data).all()
        end_date = datetime.utcnow()
        updated_data = []
        for item in query_data:
            dict_item = {x.name:getattr(item,x.name) for x in item.__table__.columns}
            dict_item.pop('record_id')
            dict_item['start_date'] = end_date
            dict_item['start_by'] = user_id if user_id else 0
            dict_item['start_mode'] = 1
            updated_data.append({**dict_item,**update_data})
        update_data['end_data'] = end_date
        self.delete_data_bulk(filter_data,user_id,end_date=end_date)
        self.add_data_bulk(updated_data)

    def check_field_exist(self,raw_field,columns):
        """
        Check if field exist in list of column names.
        """
        field_list = raw_field.split('__')
        return field_list[0] in columns

    def generate_field_filter(self,raw_field,value):
        """
        Generate SQlAlchemy filter argument with operator
        based on operator text in raw_field.
        Provided operators is 'gte','gt','lt','lte','like','in'.
        If operator is 'in', value must be a list.
        If raw_field has no operator or operator not included in provided operators,
        or operator is 'in' and value not a list, return equality operator.
        Parameters:
        -----
        raw_field:str
            field name, optionally with its operator.
            Example: name__like, field's name is 'name', operator is 'like'.
        value:obj
            value to be compared in filter_argument, it could be string or int or other.
        """
        field_list = raw_field.split('__')        
        field = getattr(self.model,field_list[0],None)
        if not field:
            return field
        if len(field_list)>1:
            if not isinstance(value,list):
                if field_list[1]=='gte':
                    return field>=value
                elif field_list[1]=='gt':
                    return field>value
                elif field_list[1]=='lt':
                    return field<value
                elif field_list[1]=='lte':
                    return field<=value
                elif field_list[1]=='like':
                    return field.like(f"%{value}%")
                elif field_list[1]=='ilike':
                    return field.ilike(f"%{value}%")
                elif field_list[1]=='ne':
                    return field!=value
            elif field_list[1]=='in':
                return field.in_(value)
        return field==value

    def generate_filter(self,query):
        """
        Generate list of filter to be used in SQLAlchemy query's filter method.
        Parameters:
        -----
        query: dict
            Dictionary of raw_field and value to be used in sql query.
        Returns: list
            Return list of filter arguments.
        """
        column_list = [item.name for item in self.model.__table__.columns]
        result = []
        for k,v in query.items():
            if self.check_field_exist(k,column_list):
                field_filter = self.generate_field_filter(k,v)
                result.append(field_filter)
        return result

    def get_columns(self,column_names):
        """
        Get list of column based on name in column_names.
        If name not found, it will be skipped.
        Parameters:
        -----
        column_names:list
            List of column name in string.
        Returns:
        -----
            List of SQLAlchemy column object.
        """
        result = []
        for item in column_names:
            try:
                result.append(getattr(self.model,item))
            except (AttributeError,TypeError):
                continue
        return result

    def generate_aggregate_query(self,raw_field_name):
        """
        Generate aggregate operator for summarizing query.
        Use it inside with_entities method.
        Parameters:
        -----
        raw_field_name:str
            field name with its operator.
        Returns:
        -----
            result of sqlalchemy func's method.
        """
        raw_field_name_list = raw_field_name.split('__')
        if len(raw_field_name_list)>1:
            fieldname = raw_field_name_list[0]
            operator = raw_field_name_list[1]
            if operator == 'count':
                return func.count(getattr(self.model,fieldname))
            elif operator == 'max':
                return func.max(getattr(self.model,fieldname))
            elif operator == 'min':
                return func.min(getattr(self.model,fieldname))
            elif operator == 'avg':
                return func.avg(getattr(self.model,fieldname))
            elif operator == 'sum':
                return func.sum(getattr(self.model,fieldname))
        else:
            fieldname = raw_field_name
            return func.count(getattr(self.model,fieldname))
