from datetime import datetime
import calendar

def get_following_month_date(initial_date,increment_month=1):
    """
    Get same date (if able) for following month(s). Example initial_date is 15/1/2020,
    add 2 month later, the date become 15/3/2020.
    If initial_date is more than following month's date, maximum date in following month is retreived.
    Parameters
    --------
    initial_date: date or datetime
        Initial date to be added
    increment_month: int
        Number of month(s) to be added to initial_date, default value is 1
    Return:datetime
    --------
    return datetime of the following month's date. If initial date is date instance and
    increment_month is 0, datetime returned will be combined from initial_date and now time.
    """
    if increment_month==0:
        if isinstance(initial_date,datetime):
            return initial_date
        else:
            now = datetime.now().time()
            return datetime.combine(initial_date,now)
    month = initial_date.month - 1 + increment_month
    year = initial_date.year + month // 12
    month = month % 12 + 1
    day = min(initial_date.day, calendar.monthrange(year,month)[1])
    next_date = datetime(year, month, day)
    return next_date

    